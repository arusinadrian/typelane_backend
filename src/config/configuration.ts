import * as config from 'config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

import { config as dotenvLoader } from 'dotenv';

dotenvLoader();

const dbConfig = config.get('db');
const serverConfig = config.get('server');

const typeOrmConfig: TypeOrmModuleOptions = {
    type: dbConfig.type,
    host: process.env.RDS_HOSTNAME || dbConfig.host,
    port: process.env.RDS_PORT || dbConfig.port,
    username: process.env.RDS_USERNAME || dbConfig.username,
    password: process.env.RDS_PASSWORD || dbConfig.password,
    database: process.env.RDS_DB_NAME || dbConfig.database,
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    synchronize: process.env.TYPEORM_SYNC || dbConfig.synchronize,
}

export const configuration = {
    typeOrmConfig,
    server: {
        port: process.env.PORT || serverConfig.port,
    },
    environment: process.env.NODE_ENV,
};
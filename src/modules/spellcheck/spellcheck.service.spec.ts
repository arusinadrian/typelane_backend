import { TestingModule, Test } from '@nestjs/testing';
import { SpellcheckService } from './spellcheck.service';
import { CorrectedTextModule } from '../correctedText/correctedText.module';
import { MissSpellModule } from '../misspell/missSpell.module';
import { OriginalTextModule } from '../originalText/originalText.module';
import { CorrectedTextService } from '../correctedText/correctedText.service';
import { MissSpellService } from '../misspell/missSpell.service';
import { OriginalTextService } from '../originalText/originalText.service';
import { SpellcheckModule } from './spellcheck.module';
import { Repository } from 'typeorm';
import { MissSpell } from '../misspell/missSpell.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configuration } from '../../config/configuration';
import { CorrectedText } from '../correctedText/correctedText.entity';
import { OriginalText } from '../originalText/originalText.entity';

const text = 'This is sme text we want to chck for typo';

class CorrectedTextServiceMock {
    async createCorrectedText() { return Promise.resolve(); }
    async getCorrectedText() { return Promise.resolve(); }
    async findCorrectedTextByOriginalTextId() { return Promise.resolve(); }
}

class MissSpellServiceMock {
    async createMisspelledWord() { return Promise.resolve(); }
    async findMisspellsOriginalTextId() { return Promise.resolve(); }
}

class OriginalTextServiceMock {
    async createOriginalText() { return Promise.resolve(); }
    async findByText() { return Promise.resolve(); }
}



describe('spellcheck service tests', () => {
    let spellcheckService: SpellcheckService;
    let missSpellService: MissSpellService;
    let originalTextService: OriginalTextService;
    let correctedTextService: CorrectedTextService;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                CorrectedTextModule,
                MissSpellModule,
                OriginalTextModule,
                SpellcheckModule,
                TypeOrmModule.forRoot(configuration.typeOrmConfig),
            ],
            providers: [SpellcheckService],
        })
        .overrideProvider(MissSpellService)
        .useClass(MissSpellServiceMock)
        .overrideProvider(CorrectedTextService)
        .useClass(CorrectedTextServiceMock)
        .overrideProvider(OriginalTextService)
        .useClass(OriginalTextServiceMock)
        .compile();

        spellcheckService = await module.get<SpellcheckService>(SpellcheckService);
        missSpellService = await module.get<MissSpellService>(MissSpellService);
        correctedTextService = await module.get<CorrectedTextService>(CorrectedTextService);
        originalTextService = await module.get<OriginalTextService>(OriginalTextService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    })

    it('should return an array of misspelled words and return corrected text', () => {
        const misspelledWords = [ 'sme', 'chck' ];
        const correctedText = 'This is time text we want to tick for typo';

        const correctText = spellcheckService.correctText(text);

        expect(correctText).toEqual({
            misspelledWords,
            correctedText
        });
    });

    it('should try to create misspeledWords, originalText and correctedText', async () => {
        const createMisspelledWord = jest.spyOn(missSpellService, 'createMisspelledWord');
        const createOriginalText = jest.spyOn(originalTextService, 'createOriginalText');
        const createCorrectedText = jest.spyOn(correctedTextService, 'createCorrectedText');

        createCorrectedText.mockResolvedValue(({ id: 1 } as CorrectedText));

        await spellcheckService.checkSpelling(text);

        expect(createMisspelledWord).toHaveBeenCalledTimes(2);
        expect(createOriginalText).toHaveBeenCalled();
        expect(createCorrectedText).toHaveBeenCalled();
    });

    it('should check whether it doesnt already have such originalText and if so return based on that', async () => {
        const findByText = jest.spyOn(originalTextService, 'findByText');
        const createMisspelledWord = jest.spyOn(missSpellService, 'createMisspelledWord');
        const createOriginalText = jest.spyOn(originalTextService, 'createOriginalText');
        const createCorrectedText = jest.spyOn(correctedTextService, 'createCorrectedText');

        const testText = 'Wery bad string';
        const correctedTextMock = {
            id: 1,
            text: "Very bad string",
        }
        const misspellsMock = [
            { id: 1, word: "Wery" },
        ];

        findByText.mockResolvedValue(({
            correctedText: correctedTextMock,
            misspells: misspellsMock,
            id: 1,
            text: testText,
        }) as OriginalText);
    
        const result = await spellcheckService.checkSpelling(text);

        expect(createMisspelledWord).not.toHaveBeenCalled();
        expect(createOriginalText).not.toHaveBeenCalled();
        expect(createCorrectedText).not.toHaveBeenCalled();
        expect(result).toEqual({
            id: correctedTextMock.id,
            misspelledWords: misspellsMock,
        });
    });
});

import { Injectable } from '@nestjs/common';
import * as SpellChecker from 'spellchecker';
import { CorrectedTextService } from '../correctedText/correctedText.service';
import { MissSpellService } from '../misspell/missSpell.service';
import { OriginalTextService } from '../originalText/originalText.service';


@Injectable()
export class SpellcheckService {
    constructor(
        private correctedTextService: CorrectedTextService,
        private missSpellService: MissSpellService,
        private originalTextService: OriginalTextService,
    ) {}
    
    private getMisspelled(corpus: string): string[] {
        const checkSpelling = SpellChecker.checkSpelling(corpus);
        const misspelledWords = checkSpelling.map(range => corpus.slice(range.start, range.end));

        return misspelledWords;
    }

    correctText(corpus: string) {
        const misspelledWords = this.getMisspelled(corpus);
        let newText = corpus;

        misspelledWords.forEach(misspell => {
            const correctlySpelledWord = SpellChecker.getCorrectionsForMisspelling(misspell)[0];
            const replaceRegex = new RegExp(misspell, 'g');
            newText = newText.replace(replaceRegex, correctlySpelledWord);
        });

        return { 
            correctedText: newText,
            misspelledWords,
        }
    }


    async checkSpelling(corpus: string) {
        const originalTextExists = await this.originalTextService.findByText(corpus);

        if (!originalTextExists) {
            const { correctedText, misspelledWords } = this.correctText(corpus);

            const misspells = await Promise.all(
                misspelledWords.map(async word => await this.missSpellService.createMisspelledWord(word))
            );

            const corrected = await this.correctedTextService.createCorrectedText(correctedText);
            await this.originalTextService.createOriginalText(corpus, misspells, corrected);

            const { id } = corrected; 

            return { 
                id,
                misspelledWords
            };
        } else {
            const { correctedText, misspells } = originalTextExists;
            return {
                id: correctedText.id,
                misspelledWords: misspells,
            };
        }
    }

    async getCorrectedText(id: string) {
        const { text } = await this.correctedTextService.getCorrectedText(+id);

        return text;
    }
}

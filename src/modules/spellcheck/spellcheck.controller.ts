import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { SpellcheckService } from './spellcheck.service';
import { SpellcheckDto } from './dto/spellcheck.dto';

@Controller('spellcheck')
export class SpellcheckController {
    constructor(private readonly spellcheckService: SpellcheckService) {}

    @Post()
    async checkSpelling(@Body() spellcheckDto: SpellcheckDto) {
        return this.spellcheckService.checkSpelling(spellcheckDto.text);
    }

    @Get(':id')
    async getFixedSentence(@Param('id') id: string) {
        const text = await this.spellcheckService.getCorrectedText(id);
        return  { text };
    }
}
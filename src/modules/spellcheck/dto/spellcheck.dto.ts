import { IsNotEmpty } from 'class-validator';

export class SpellcheckDto {
    @IsNotEmpty()
    text: string
}
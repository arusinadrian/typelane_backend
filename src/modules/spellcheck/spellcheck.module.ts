import { Module } from '@nestjs/common';
import { SpellcheckController } from './spellcheck.controller';
import { SpellcheckService } from './spellcheck.service';
import { CorrectedTextModule } from '../correctedText/correctedText.module';
import { MissSpellModule } from '../misspell/missSpell.module';
import { OriginalTextModule } from '../originalText/originalText.module';

@Module({
    imports: [
        CorrectedTextModule,
        MissSpellModule,
        OriginalTextModule,
    ],
    controllers: [SpellcheckController],
    providers: [SpellcheckService],
})
export class SpellcheckModule {}

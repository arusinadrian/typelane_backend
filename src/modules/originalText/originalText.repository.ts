import { EntityRepository, Repository } from 'typeorm';
import { OriginalText } from './originalText.entity';
import { MissSpell } from '../misspell/missSpell.entity';
import { Logger, InternalServerErrorException } from '@nestjs/common';
import { CorrectedText } from '../correctedText/correctedText.entity';

@EntityRepository(OriginalText)
export class OriginalTextRepository extends Repository<OriginalText> {
    async createOriginalText(text: string, misspells: MissSpell[], correctedText: CorrectedText) {
        const originalText = new OriginalText();
        Object.assign(originalText, {
            text, 
            misspells,
            correctedText,
        });

        try {
            await originalText.save();
        } catch (error) {
            Logger.error(`Failed to create originalText for text: \n ${text}.`);
            throw new InternalServerErrorException();
        }

        return originalText;
    }

    async findByText(text: string) {
        try {
            const originalText = await this.find({
                relations: ['correctedText', 'misspells'],
                where: { 
                    text,
                }
            });

            return originalText[0];
        } catch (error) {
            Logger.debug(`Failed to find original text for: ${text}`);
        }
    }
}
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany, Unique, Index, OneToOne, JoinColumn } from 'typeorm';
import { MissSpell } from '../misspell/missSpell.entity';
import { CorrectedText } from '../correctedText/correctedText.entity';


@Entity()
export class OriginalText extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Index({ unique: true })
    @Column()
    text: string;

    @OneToMany(type => MissSpell, missSpell => missSpell.originalText)
    misspells: MissSpell[];

    @OneToOne(type => CorrectedText)
    @JoinColumn()
    correctedText: CorrectedText;
}
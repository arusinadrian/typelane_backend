import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OriginalTextRepository } from './originalText.repository';
import { OriginalTextService } from './originalText.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([OriginalTextRepository]),
    ],
    providers: [OriginalTextService],
    exports: [OriginalTextService],
})
export class OriginalTextModule {}

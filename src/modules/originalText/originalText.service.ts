import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OriginalTextRepository } from './originalText.repository';
import { OriginalText } from './originalText.entity'
import { MissSpell } from '../misspell/missSpell.entity';
import { CorrectedText } from '../correctedText/correctedText.entity';

@Injectable()
export class OriginalTextService {
    constructor(
        @InjectRepository(OriginalTextRepository)
        private originalTextRepository: OriginalTextRepository,
    ) {}

    async createOriginalText(text: string, misspells: MissSpell[], correctedText: CorrectedText): Promise<OriginalText> {
        return this.originalTextRepository.createOriginalText(text, misspells, correctedText);
    }

    async findByText(text:string) {
        return await this.originalTextRepository.findByText(text);
    }
}
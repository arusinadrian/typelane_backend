import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { OriginalText } from '../originalText/originalText.entity';

@Entity()
export class MissSpell extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    word: string;

    @ManyToOne(type => OriginalText, originalText => originalText.misspells)
    originalText: OriginalText;
}
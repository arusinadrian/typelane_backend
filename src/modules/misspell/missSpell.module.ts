import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MissSpellRepository } from './missSpell.repository';
import { MissSpellService } from './missSpell.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([MissSpellRepository]),
    ],
    providers: [MissSpellService],
    exports: [MissSpellService],
})
export class MissSpellModule {}
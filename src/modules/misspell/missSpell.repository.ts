import { EntityRepository, Repository } from 'typeorm';
import { MissSpell } from './missSpell.entity';
import { Logger, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(MissSpell)
export class MissSpellRepository extends Repository<MissSpell> {
    async createMisspelledWord(word: string) {
        const missSpell = new MissSpell();
        missSpell.word = word;

        try {
            await missSpell.save();
        } catch (error) {
            Logger.error(`Failed to create a misspelled word for ${word}.`);
            throw new InternalServerErrorException();
        }

        return missSpell;
    }
}
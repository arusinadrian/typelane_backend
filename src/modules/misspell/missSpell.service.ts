import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MissSpellRepository } from './missSpell.repository';
import { MissSpell } from './missSpell.entity';

@Injectable()
export class MissSpellService {
    constructor(
        @InjectRepository(MissSpellRepository)
        private missSpellRepository: MissSpellRepository,
    ) {}

    async createMisspelledWord(word: string) {
        return this.missSpellRepository.createMisspelledWord(word);
    }
}
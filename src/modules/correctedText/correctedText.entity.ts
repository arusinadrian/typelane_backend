import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';

@Entity()
export class CorrectedText extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    text: string;
}
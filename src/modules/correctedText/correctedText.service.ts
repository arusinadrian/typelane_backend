import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CorrectedTextRepository } from './correctedText.repository';
import { CorrectedText } from './correctedText.entity';
import { OriginalText } from '../originalText/originalText.entity';

@Injectable()
export class CorrectedTextService {
    constructor(
        @InjectRepository(CorrectedTextRepository)
        private correctedTextRepository: CorrectedTextRepository,
    ) {}

    async createCorrectedText(text: string): Promise<CorrectedText> {
        return this.correctedTextRepository.createCorrectedText(text);
    }

    async getCorrectedText(id: number) {
        return this.correctedTextRepository.findOne(id);
    }
}
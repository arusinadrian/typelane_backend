import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CorrectedTextRepository } from './correctedText.repository';
import { CorrectedTextService } from './correctedText.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([CorrectedTextRepository]),
    ],
    providers: [CorrectedTextService],
    exports: [CorrectedTextService],
})
export class CorrectedTextModule {} 

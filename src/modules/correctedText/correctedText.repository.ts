import { EntityRepository, Repository } from 'typeorm';
import { CorrectedText } from './correctedText.entity';
import { OriginalText } from '../originalText/originalText.entity';
import { Logger, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(CorrectedText)
export class CorrectedTextRepository extends Repository<CorrectedText> {
    async createCorrectedText(text: string) {
        const correctedText = new CorrectedText();
        
        Object.assign(correctedText, {
            text,
        });

        try {
            await correctedText.save();
        } catch (error) {
            Logger.error(`Failed to create correctedText for text ${text}.`);
            throw new InternalServerErrorException();
        }

        return correctedText
    }

    async findCorrectedText() {
        const query = this.createQueryBuilder('corrected_text');

        query.leftJoinAndSelect('corrected_text.originalText', 'originalText')
    }

}
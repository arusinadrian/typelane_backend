import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SpellcheckModule } from './modules/spellcheck/spellcheck.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configuration } from './config/configuration';
import { CorrectedTextModule } from './modules/correctedText/correctedText.module';
import { MissSpellModule } from './modules/misspell/missSpell.module';
import { OriginalTextModule } from './modules/originalText/originalText.module';

@Module({
  imports: [
    SpellcheckModule,
    CorrectedTextModule,
    MissSpellModule,
    OriginalTextModule,
    TypeOrmModule.forRoot(configuration.typeOrmConfig),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

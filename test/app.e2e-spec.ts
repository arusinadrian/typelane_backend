import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('/ (POST) spellcheck', () => {
    return request(app.getHttpServer())
      .post('/spellcheck')
      .send({
        text: 'This is sme text we want to chck for typo',
      })
      .expect(201)
      .expect({
        id: 1,
        misspells: [ 'sme', 'chck' ]
      });
  });

  it('/ (GET) spellfix', () => {
    return request(app.getHttpServer())
      .get('/spellfix')
      .query({ id: '1' })
      .expect(200)
      .expect('This is time text we want to tick for typo');
  });
});
